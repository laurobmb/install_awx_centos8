### Sumário

* [Como instalar o Ansible AWX no CentOS 8](http://gitlab.conectado.local/lpgomes/install_awx#como-instalar-o-ansible-awx-no-centos-8)
* [Pré Requisitos](http://gitlab.conectado.local/lpgomes/install_awx#pr%C3%A9-requisitos)
* [Instale o Docker e o Docker Compose](http://gitlab.conectado.local/lpgomes/install_awx#instale-o-docker-e-o-docker-compose)
* [Instale o Ansible AWX](http://gitlab.conectado.local/lpgomes/install_awx#instale-o-ansible-awx)
* [Configure o SELinux e o FirewallD](http://gitlab.conectado.local/lpgomes/install_awx#configure-o-selinux-e-o-firewalld)
* [Acesso ao AWX Web Interface](http://gitlab.conectado.local/lpgomes/install_awx#acesso-ao-awx-web-interface)
* [Conclusão](http://gitlab.conectado.local/lpgomes/install_awx#conclus%C3%A3o)

# Como instalar o Ansible AWX no CentOS 8

AWX significa "Ansible Web eXecutable" é um projeto gratuito e de código aberto que permite gerenciar e controlar seu projeto Ansible facilmente. Ele fornece uma interface de usuário baseada na web e um mecanismo de tarefas criado com base no Ansible. Ele fornece uma API REST poderosa e permite que você gerencie ou sincronize o inventário com outras fontes de nuvem, controle o acesso e integre com o LDAP.
Neste tutorial, mostraremos como instalar o Ansible AWX com Docker no CentOS 8.

### Pré Requisitos
Um servidor executando CentOS 8 com no mínimo 4 GB de RAM.
Uma senha de root é configurada em seu servidor.
Iniciando
Antes de começar, você precisará instalar o repositório EPEL em seu sistema. Você pode instalá-lo com o seguinte comando:

> dnf install epel-release -y

Em seguida, você precisará instalar alguns pacotes adicionais necessários para executar o AWX em seu sistema. Você pode instalar todos eles com o seguinte comando:

> dnf install git gcc gcc-c ++ ansible nodejs gettext device-mapper-persistent-data lvm2 bzip2 python3-pip -y

Depois que todos os pacotes estiverem instalados, você pode prosseguir para a próxima etapa.

### Instale o Docker e o Docker Compose

Em seguida, você precisará instalar o Docker para executar o AWX dentro do contêiner do Docker. Por padrão, a versão mais recente do Docker não está disponível no repositório padrão do CentOS 8. Portanto, você precisará adicionar um repositório Docker em seu sistema. Você pode adicionar o repositório Docker com o seguinte comando:

> dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo

Depois de adicionado, instale a versão estável mais recente do Docker com o seguinte comando:

> dnf install docker-ce-3:18.09.1-3.el7 -y

Depois de instalar o Docker, verifique a versão instalada do Docker com o seguinte comando:

> docker --version

Você deve obter a seguinte saída ou semelhante: Docker versão 19.03.7, build 7141c199a2

Em seguida, inicie o serviço Docker e ative-o para iniciar após a reinicialização do sistema com o seguinte comando:

> systemctl start docker
> systemctl enable docker

Você pode verificar o status do serviço Docker com o seguinte comando:

> systemctl status docker


Em seguida, instale o Docker compose usando o seguinte comando:

> pip3 install docker-compose

Depois de instalado, verifique a versão do Docker compose usando o seguinte comando:

> docker-compose --version

Por fim, execute o seguinte comando para definir o comando python para usar o python 3:

> alternatives --set python /usr/bin/python3

### Instale o Ansible AWX

Primeiro, baixe a versão mais recente do Ansible AWX do repositório Git Hub usando o seguinte comando:

> git clone https://github.com/ansible/awx.git

Em seguida, gere uma chave secreta para criptografar o arquivo de inventário com o seguinte comando:

> openssl rand -base64 30

Essa deve ser a saída do comando ou algo parecido: R+kbcDEUS8DlAftAbfWafVqLZ0lUy+Paqo4fEtgp

Nota: Lembre-se dos segredos acima, você precisará colocá-lo no arquivo de inventário. Em seguida, vá no diretório awx/installer/ e edite o arquivo de inventário:

> vim awx/installer/inventory

Altere as seguintes linhas:

```
[all:vars]
dockerhub_base=ansible
awx_task_hostname=awx
awx_web_hostname=awxweb
postgres_data_dir="/var/lib/pgdocker"
host_port=80
host_port_ssl=443
docker_compose_dir="~/.awx/awxcompose"
pg_username=awx
pg_password=awxpass
pg_database=awx
pg_port=5432
pg_admin_password=password
rabbitmq_password=awxpass
rabbitmq_erlang_cookie=cookiemonster
admin_user=admin
admin_password=password
create_preload_data=True
secret_key=R+kbcDEUS8DlAftAbfWafVqLZ0lUy+Paqo4fEtgp
awx_official=true
awx_alternate_dns_servers="8.8.8.8,8.8.4.4"
project_data_dir=/var/lib/awx/projects
```

Salve e feche o arquivo quando terminar. Em seguida, crie um diretório para Postgres:

> mkdir /var/lib/pgdocker

Por fim, execute o seguinte comando para instalar o AWX:

> ansible-playbook -i inventory install.yml

### Configure o SELinux e o FirewallD
Por padrão, o SELinux está habilitado no CentOS 8. É recomendado desabilitá-lo para funcionar AWX no ambiente Docker. Você pode desativá-lo editando o seguinte arquivo:

```
vim /etc/sysconfig/selinux
SELINUX=disabled
```

Salve e feche o arquivo quando terminar. Em seguida, reinicie o sistema para implementar as alterações. Em seguida, você precisará permitir o serviço http e https por meio do firewalld. Você pode permiti-los com o seguinte comando:

```
firewall-cmd --zone=public --add-masquerade --permanent
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
```

Em seguida, reinicie o serviço firewalld para aplicar as alterações:

> firewall-cmd --reload

### Acesso ao AWX Web Interface
Agora, abra seu navegador da web e digite a URL http: // seu-ip-do-servidor. Você será redirecionado para a página de login do AWX: 

> http://192.168.123.227/#/login

![](fotos/awx1.png)

Forneça seu nome de usuário e senha de administrador que você definiu no arquivo de inventário e clique no botão SIGN IN. Você deve ver o painel padrão do AWX na página a seguir:

![](fotos/awx2.png)

### Conclusão
Parabéns! você instalou com êxito o AWX com Docker no CentOS 8. Agora você pode gerenciar e controlar seu projeto Ansible facilmente usando a interface da web do AWX.

Fontes: 

* [HowtoForge](https://www.howtoforge.com/tutorial/centos-ansible-awx-installation/)
* [AWX](https://github.com/ansible/awx)
* [Docker](https://docs.docker.com/)